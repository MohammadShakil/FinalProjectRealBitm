<?php
namespace App\Bitm\SEIP123473\ProfilePicture;
use App\Bitm\SEIP123473\Message\Message;
class ImageUploader
{
    public $id="";
    public $name="";
    public $image_name="";
    public $conn="";
    public $deleted_at;



    public function prepare($data=array())
{
    if(array_key_exists("name",$data))
    {
        $this->name=$data["name"];
    }
    if(array_key_exists("image",$data))
    {
        $this->image_name=$data["image"];
    }
    if(array_key_exists("id",$data))
    {
        $this->id=$data["id"];
    }
    return $this;

}


    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","atomicproject21")or die("Data connection is not connected successfully");
    }


    public function store()
    {
        $query="INSERT INTO `atomicproject21`.`profilepicture` ( `name`, `images`) VALUES ( '".$this->name."', '".$this->image_name."')";
        $result=mysqli_query($this->conn,$query);
        if($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            header("location:index.php");

        }


        else
        {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has not been stored successfully.
</div>");
            header("location:index.php");
        }

    }


    public function index()
    {
        $allInfo=array();
        $query="SELECT * FROM `profilepicture` WHERE `deleted_at` IS NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $allInfo[]=$row;
        }
        return $allInfo;


    }


    public function view()
    {
        $query="SELECT * FROM `profilepicture` WHERE `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }




    public function update()
    {
        $query="UPDATE `atomicproject21`.`profilepicture` SET `name` = '".$this->name."', `images` = '".$this->image_name."' WHERE `profilepicture`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result) {
            Message::message("<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated successfully.
</div>");
            header("location:index.php");

        }


        else
        {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has not been updated successfully.
</div>");
            header("location:index.php");
        }

    }

    public function delete()
    {
        $query="DELETE FROM `atomicproject21`.`profilepicture` WHERE `profilepicture`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been deleted successfully.
</div>");
            header("location:index.php");

        }


        else
        {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has not been deleted successfully.
</div>");
            header("location:index.php");
        }

    }

    public function trash()
    {
        $query="UPDATE `atomicproject21`.`profilepicture` SET `deleted_at` = '".$this->deleted_at."' WHERE `profilepicture`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been trashed successfully.
</div>");
            header("location:index.php");

        }


        else
        {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has not been trashed successfully.
</div>");
            header("location:index.php");
        }

    }


    public function trashed()
    {
        $allInfo=array();
        $query="SELECT * FROM `profilepicture` WHERE `deleted_at` IS NOT NULL";
        $result=mysqli_query($this->conn,$query);
        while($row=mysqli_fetch_assoc($result))
        {
            $allInfo[]=$row;
        }
        return $allInfo;

    }




    public function recover()
    {
        $query="UPDATE `atomicproject21`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if($result) {
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been recovered successfully.
</div>");
            header("location:index.php");

        }


        else
        {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong> Data has not been recovered successfully.
</div>");
            header("location:index.php");
        }

    }



    public function recoverMultiple($idS=array())
    {
        if((is_array($idS))&&(count($idS)>0)) {
            $IDs =implode(",",$idS);
            $query = "UPDATE `atomicproject21`.`profilepicture` SET `deleted_at` = NULL WHERE `profilepicture`.`id` IN (" .$IDs.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("<div class=\"alert alert-info\">
  <strong>Recovered!</strong>Selected data has been recovered successfully.
</div>");
                header('Location:index.php');

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong>Selected data has not been recovered  successfully.
    </div>");
                header('Location:index.php');
            }
        }


    }




    public function deleteMultiple($idS=array())
    {
        if((is_array($idS))&&(count($idS>0))) {
            $IDs=implode(",",$idS);
            $query = "DELETE FROM `atomicproject21`.`profilepicture` WHERE `profilepicture`.`id` IN (".$IDs.")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong>Selected data has been deleted successfully.
</div>");
                header("location:index.php");

            } else {
                Message::message("<div class=\"alert alert-danger\">
  <strong>Failed!</strong>Selected data has not been deleted successfully.
</div>");
                header("location:index.php");
            }
        }

    }





}


?>