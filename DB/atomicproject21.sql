-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2016 at 09:20 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicproject21`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `birthdate` date NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthdate`, `deleted_at`) VALUES
(2, 'shakil', '2022-08-20', '1468946558'),
(5, 'Salman', '0000-00-00', '1467713678'),
(8, 'khan', '1999-02-12', NULL),
(9, 'shakil', '2012-02-22', NULL),
(10, 'aSNKLASjlas', '2012-02-20', NULL),
(11, 'shakillll', '2222-02-20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`) VALUES
(11, 'shakil', NULL),
(13, 'Yo Yo', NULL),
(14, 'gfhgfhgfhg', NULL),
(15, 'sskdsajk', NULL),
(16, 'sd', '1468908668'),
(17, 'asdasds', NULL),
(18, 'shakilllllll', NULL),
(19, 'shakil khan khan', NULL),
(20, 'sanjit', '1468908724'),
(21, 'sdsdsdsdd', '1468820931'),
(22, 'sxhsjdhaskjdhakjshdakjh', '1468822497'),
(23, 'khna', '1468821379'),
(24, 'b', '1468841548'),
(25, 'sanjit', '1468908947'),
(26, 'pp', '1468827894'),
(27, 'vvvv', '1468839879'),
(28, 'a,mi', NULL),
(29, 'shskhdkhas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`, `deleted_at`) VALUES
(21, 'Shakil', 'Chittagong', NULL),
(22, 'mostofa', 'Dhaka', NULL),
(23, 'khan', 'Maijdee', NULL),
(24, 'ami', 'Jamalpur', NULL),
(25, 'khan', 'Gagipur', '1467713504'),
(26, 'pc', 'Chittagong', '1467713520'),
(27, 'jskhdjkshd', 'Dhaka', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_address` varchar(60) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email_address`, `deleted_at`) VALUES
(2, 'shakil.khan12000@gmail.com', '1467713952'),
(3, 'Shakilkhanzz12@gmail.com', NULL),
(4, 'dkhskdh@gmail.com', NULL),
(5, 'shhshs@gmail.com', NULL),
(6, 'khan@gmail.com', NULL),
(7, 'aa@gmail.com', NULL),
(8, 'khan@ami.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `gender_type` varchar(40) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender_type`, `deleted_at`) VALUES
(14, 'amir', 'Male', '1467135252'),
(18, 'nihan', 'Male', '1467135254'),
(20, 'Shakil', 'Male', NULL),
(21, 'mostofa', 'Male', '1467713818'),
(22, 'salman', 'Male', NULL),
(23, 'Hira', 'Female', '1467713816'),
(24, 'kaonon', 'Male', NULL),
(25, 'lucky', 'Female', NULL),
(26, 'jaitun', 'Female', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `hobby` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `firstname`, `lastname`, `hobby`, `deleted_at`) VALUES
(42, 'shakil', 'khan', 'Coding,Gardening', NULL),
(43, 'tin', 'tin', 'Coding,Singing', NULL),
(44, 'BooM', 'BooM', 'Singing', ''),
(45, 'Mahpara ', 'Labiba', 'Coding,Browsing,Singing,Gardening,playing Football', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE IF NOT EXISTS `profilepicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(41, 'Shakil', '1467714024_MG_0234 log sll.jpg', NULL),
(43, '', '1467714086_MG_6813new replace-2sll.jpg', NULL),
(44, '', '146771409912310419_858921187561879_8886590583901152676_n.jpg', ''),
(46, 'kill', '1467714130_MG_0256 log smll.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `textarea`
--

CREATE TABLE IF NOT EXISTS `textarea` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `summery` varchar(600) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `textarea`
--

INSERT INTO `textarea` (`id`, `name`, `summery`, `deleted_at`) VALUES
(14, 'promi', 'I am entering some names and their email address in the textarea, and everytime i echo the value or textarea it skips the email address and only showing the name let me show the way i am entering value in textarea', '1467117295'),
(19, 'shakil khan', 'Hellow World !! ', NULL),
(20, 'kjhan', 'Hey!!', NULL),
(21, 'Linkon', 'Go Ahead....', NULL),
(22, 'BDBOY', 'Bangladeshi !!', '1467714385'),
(23, 'khan', 'Hey!!', NULL),
(24, 'khan', 'Hey!!', NULL),
(26, 'khan', 'Hey!!', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
