<?php
require_once('../../../vendor/mpdf/mpdf/mpdf.php');
require_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP123473\Email\Email;
$obj= new Email();
$allData= $obj->index();
//var_dump($allData);
//die();

$trs="";
$sl=0;
foreach($allData as $data):
    $sl++;
    $trs.="<tr>";
    $trs.="<td>$sl</td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['email_address']."</td>";
    $trs.="</tr>";
endforeach;

//echo $trs;
//die();

$html=<<<EOD
<!DOCTYPE html>
<html>
<head>
</head>
<body>

<div class="container">
  <h2>Book List</h2>
   <table class="table">
    <thead>
      <tr>
        <th>SL#</th>
        <th>ID</th>
        <th>Email</th>

      </tr>
    </thead>
    <tbody>
        $trs
    </tbody>

</table>
</body>
</html>
EOD;


$mpdf = new mPDF();

// Write some HTML code:

$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output("list.pdf",'D');