<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resoureces/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../Resoureces/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Email</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <label for="email">Enter Email Address:</label>
            <input type="email" name="email" class="form-control" id="email" placeholder="Enter your email">
        </div><br>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>

</body>
</html>
