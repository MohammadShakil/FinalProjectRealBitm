<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Gender\Gender;


$gender=new Gender();
$trashedItem=$gender->trashed();
?>




<!DOCTYPE html>
<html lang="en">
<head>
    <title>Gender</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Trash List</h2>
    <a href="index.php" class="btn btn-primary" role="button">View Index List</a><br><br>
    <form action="recoverMultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-info">Recover Selected Items</button>
        <button type="submit" class="btn btn-info" id="delete">Delete Selected Items</button><br><br>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Select</th>
                <th>#SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Gender</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $s=0;
            foreach($trashedItem as $trash){
                $s++;
                ?>
                <tr>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $trash["id"]?>"> </td>
                    <td><?php echo $s?></td>
                    <td><?php echo $trash["id"]?></td>
                    <td><?php echo $trash["name"]?></td>
                    <td><?php echo $trash["gender_type"]?></td>
                    <td><a href="recover.php?id=<?php echo $trash["id"]?>" class="btn btn-warning" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $trash["id"]?>" class="btn btn-danger" role="button">Delete</a>
                    </td>

                </tr>
            <?php } ?>

            </tbody>
        </table>
    </form>
</div>
<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
</script>
</body>
</html>

