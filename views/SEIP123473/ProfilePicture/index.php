<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\ProfilePicture\ImageUploader;
use App\Bitm\SEIP123473\Message\Message;

$profilepicture=new ImageUploader();
$allInfo=$profilepicture->index();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../../../index.php" class="btn btn-danger square-btn-adjust">Home</a> </div>

<div class="container">
    <h2>View User Information</h2>
    <a href="create.php" class="btn btn-info" role="button">Add Profile Picture</a>
    <a href="trashed.php" class="btn btn-info" role="button">View Trashed List</a>


    <div id="message">
        <?php
        if((array_key_exists("message",$_SESSION))&&(!empty($_SESSION["message"]))) {
            echo Message::message();
        }
        ?>
    </div>



    <table class="table">
        <thead>
        <tr>
            <th>#SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Images</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $s=0;
        foreach($allInfo  as $info)
        {
            $s++;

            ?>
            <tr class="success">
                <td><?php echo $s ?></td>
                <td><?php echo $info["id"]?></td>
                <td><?php echo $info["name"]?></td>
                <td><img src="../../../Resources/images/<?php echo $info["images"]?>" alt="image" width="100px" height="100px" </td>
                <td> <a href="view.php?id=<?php echo $info["id"]?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $info["id"]?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $info["id"]?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $info["id"]?>" class="btn btn-warning" role="button">Trash</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>




