<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\ProfilePicture\ImageUploader;


$profilepicture=new ImageUploader();
$trashedItems=$profilepicture->trashed();
?>



<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Profile Picture</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Trashed List</h2>
    <a href="index.php" class="btn btn-info" role="button">View Index</a><br><br>

    <form action="recoverMultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-primary">Recover Selected Items</button>
        <button type="submit" class="btn btn-danger" id="delete">Delete Selected Items</button><br><br>
        <table class="table">
            <thead>
            <tr>
                <th>#SL</th>
                <th>ID</th>
                <th>Name</th>
                <th>Images</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $s=0;
            foreach($trashedItems  as $trashed)
            {
                $s++;

                ?>
                <tr class="success">
                    <td><input type="checkbox" name="mark[]" value="<?php echo $trashed["id"]?>"> </td>
                    <td><?php echo $s ?></td>
                    <td><?php echo $trashed["id"]?></td>
                    <td><?php echo $trashed["name"]?></td>
                    <td><img src="../../../Resources/images/<?php echo $trashed["images"]?>" alt="image" width="100px" height="100px" </td>
                    <td>
                        <a href="delete.php?id=<?php echo $trashed["id"]?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="recover.php?id=<?php echo $trashed["id"]?>" class="btn btn-warning" role="button">Recover</a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </form>
</div>
<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
</script>

</body>
</html>



