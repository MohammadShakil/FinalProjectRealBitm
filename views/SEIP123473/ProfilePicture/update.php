<?php
include_once ('../../../vendor/autoload.php');
use App\Bitm\SEIP123473\Utility\Utility;
use App\Bitm\SEIP123473\ProfilePicture\ImageUploader;


$profilePicture=new ImageUploader();
$singleInfo=$profilePicture->prepare($_POST)->view();

if((isset($_FILES["image"]))&&(!empty($_FILES["image"]["name"])))
{
    $imageName=time().$_FILES["image"]["name"];
    $temporaryLocation=$_FILES["image"]["tmp_name"];
    unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjectB21/Resources/Images/'.$singleInfo['images']);
    move_uploaded_file($temporaryLocation,"../../../Resources/images/".$imageName);
    $_POST["image"]=$imageName;

    //Utility::d($_POST);

    $profilePicture=new ImageUploader();
    $profilePicture->prepare($_POST)->update();
}