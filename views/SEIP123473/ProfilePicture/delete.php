<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\ProfilePicture\ImageUploader;

$profilepicture=new ImageUploader();
$singleInfo=$profilepicture->prepare($_GET)->view();
unlink($_SERVER['DOCUMENT_ROOT'].'/AtomicProjectB21/Resources/images/'.$singleInfo['images']);
$profilepicture->prepare($_GET)->delete();
?>