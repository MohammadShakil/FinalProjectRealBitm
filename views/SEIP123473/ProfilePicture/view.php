<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\profilepicture\ImageUploader;


$profilepicture=new ImageUploader();
$allInfo=$profilepicture->prepare($_GET)->view();
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Image Uploader</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Individual Info </h2>
    <ul class="list-group">
        <li class="list-group-item list-group-item-success">ID:<?php echo $allInfo["id"]?></li>
        <li class="list-group-item list-group-item-info">Name:<?php echo $allInfo["name"]?></li>
        <li class="list-group-item list-group-item-warning">Profile Picture:<img src="../../../Resources/images/<?php echo $allInfo["images"]?>" alt="image" height="100px" width="100px" class="img-responsive">
        </li>

    </ul>

</div>

</body>
</html>





