<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select your hobby</h2>
    <form role="form" action="store.php" method="post" ">
       <div> First name: <input type="text" name="fname" placeholder="Enter your firstname"></div><br>
        <div>Last name: <input type="text" name="lname"placeholder="Enter your last name"></div><br>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Coding">Coding</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="Hobby[]" value="Browsing">Browsing</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="Hobby[]" value="Singing" >Singing</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="Hobby[]" value="Gardening">Gardening</label>
        </div>
        <div class="checkbox disabled">
            <label><input type="checkbox" name="Hobby[]" value="playing Football">playing Football</label>
        </div>
        <div><input type="submit" value="Submit"></div>
    </form>
</div>

</body>
</html>

