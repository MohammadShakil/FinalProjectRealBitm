<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Hobby\Hobby;

$hobby=new Hobby();
$allinfo=$hobby->prepare($_GET)->view();
?>


 <html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View</h2>
    <ul class="list-group">
        <li class="list-group-item">ID:  <?php echo $allinfo["id"]?></li>
        <li class="list-group-item">First Name:  <?php echo $allinfo["firstname"]?></li>
        <li class="list-group-item">Last Name: <?php echo $allinfo["lastname"]?></li>
        <li class="list-group-item">Hobby:  <?php echo $allinfo["hobby"]?></li>
    </ul>
</div>

</body>
</html>

