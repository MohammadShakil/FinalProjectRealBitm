<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Hobby\Hobby;
use App\Bitm\SEIP123473\Utility\Utility;

$hobby=new Hobby();
$singleInfo=$hobby->prepare($_GET)->view();
//Utility::dd($singleInfo);
$singleInfoList=$singleInfo["hobby"];
$singleInfoArray=explode(",",$singleInfoList);

?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hobby</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select your hobby</h2>
    <form role="form" action="update.php" method="post" ">
    <input type="hidden" name="id"  value="<?php echo $singleInfo["id"]?>"></div><br>
    <div> First name: <input type="text" name="fname"  value="<?php echo $singleInfo["firstname"]?>"></div><br>
    <div>Last name: <input type="text" name="lname" value="<?php echo $singleInfo["lastname"]?>"></div><br>
    <div class="checkbox">
        <label><input type="checkbox" name="Hobby[]" value="Coding" <?php if(in_array("Coding",$singleInfoArray)){echo "checked";}else{echo "";}?>>Coding</label>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="Hobby[]" value="Browsing" <?php if(in_array("Browsing",$singleInfoArray)){echo "checked";}else{echo "";}?>>Browsing</label>
    </div>
    <div class="checkbox ">
        <label><input type="checkbox" name="Hobby[]" value="Singing" <?php if(in_array("Singing",$singleInfoArray)){echo "checked";}else{echo "";}?> >Singing</label>
    </div>
    <div class="checkbox ">
        <label><input type="checkbox" name="Hobby[]" value="Gardening" <?php if(in_array("Gardening",$singleInfoArray)){echo "checked";}else{echo "";}?>>Gardening</label>
    </div>
    <div class="checkbox ">
        <label><input type="checkbox" name="Hobby[]" value="Playing Football" <?php if(in_array("Playing Football",$singleInfoArray)){echo "checked";}else{echo "";}?>>playing Football</label>
    </div>
    <div><input type="submit" value="Update"></div>
    </form>
</div>

</body>
</html>

