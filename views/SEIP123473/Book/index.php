<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Book\Book;
use App\Bitm\SEIP123473\Message\Message;
use App\Bitm\SEIP123473\Utility\Utility;
$book= new Book();
//$allTitle=$title->index();
$totalItem=$book->count();
if(array_key_exists('itemPerPage',$_SESSION)){
    if(array_key_exists('itemPerPage',$_GET)){
        $_SESSION['itemPerPage']=$_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

$itemPerPage= $_SESSION['itemPerPage'];


$noOfPage= ceil($totalItem/$itemPerPage);
//Utility::d($noOfPage);
$pagination="";
if(array_key_exists('pageNumber',$_GET)){
    $pageNo=$_GET['pageNumber'];
}
else{
    $pageNo=1;
}
for($i=1;$i<=$noOfPage;$i++){
    $active=($pageNo==$i)?"active":"";
    $pagination.="<li class='$active'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNo-1);

$allBook=$book->paginator($pageStartFrom,$itemPerPage);
$prev=$pageNo-1;
$next=$pageNo+1;
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
<div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../../../index.php" class="btn btn-danger square-btn-adjust">Home</a> </div>


<div class="container">
    <h2>Book List</h2>

    <a href="create.php" class="btn btn-info" role="button">Add Book Title</a>
    <a href="trashed_view.php" class="btn btn-primary" role="button">Trashed List</a><br><br>
    <a href="pdf.php" class="btn btn-info" role="button">Download as PDF</a>
    <a href="xl.php" class="btn btn-info" role="button">Download as XL</a>
    <a href="mail.php" class="btn btn-info" role="button">Email to friend</a>

    <br>
    <div id="message">

        <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message']))){
            echo Message::message();
        }?>
    </div>
    <form role="form" action="index.php">
        <div class="form-group">
            <label for="sel1">Select list (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option>5</option>
                <option>10</option>
                <option selected>15</option>
                <option>20</option>
                <option>25</option>
            </select>
            <button type="submit">Go!</button>

    </form>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Book Title</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allBook as $book){
                $sl++;
                ?>
                <tr>
                    <td><?php echo $sl+$pageStartFrom; ?></td>
                    <td><?php echo $book['id'] // for object: $book->id ; ?></td>
                    <td><?php echo $book['title'] // for object: $book->title; ?></td>
                    <td>
                        <a href="view.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">View</a>
                        <a href="edit.php?id=<?php echo $book['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $book['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $book['id']?>" class="btn btn-info  btn-xs" role="button">Trash</a>
                    </td>


                </tr>
            <?php } ?>


            </tbody>
        </table>
        <ul class="pagination">
            <?php if($pageNo>1){echo "<li><a href='index.php?pageNumber=$prev'>Prev</a></li>";}else{echo "";}?>
            <?php echo $pagination?>
            <?php if($pageNo<$noOfPage){echo "<li><a href='index.php?pageNumber=$next'>Next</a></li>";}else{echo "";}?>
        </ul>
    </div>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>
