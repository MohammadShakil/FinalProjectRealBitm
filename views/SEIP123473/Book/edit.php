<?php

include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Book\Book;
$title=new Book();
$singleItem=$title->prepare($_GET)->view();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../../Resoureces/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../../../Resoureces/bootstrap/js/bootstrap.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Email</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label>Edit Book Title:</label>
            <input type="hidden" name="id"  value="<?php echo $singleItem["id"]?>">
            <input type="text" name="Title" class="form-control" value="<?php echo $singleItem["title"]?>">
        </div><br>
        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
