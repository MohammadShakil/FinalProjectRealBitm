<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Birthday\Birthday;

$birthday=new Birthday();
$allInfo=$birthday->prepare($_GET)->view();
?>


<html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>View Birthday List</h2>
    <ul class="list-group">
        <li class="list-group-item">ID:  <?php echo $allInfo["id"]?></li>
        <li class="list-group-item">Name: <?php echo $allInfo["name"]?></li>
        <li class="list-group-item">Birthday:  <?php echo $allInfo["birthdate"]?></li>
    </ul>
</div>

</body>
</html>

