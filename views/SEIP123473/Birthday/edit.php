<?php

include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Birthday\Birthday;
$birthday=new Birthday();
$singleItem=$birthday->prepare($_GET)->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Atomic Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Birthday</h2>
    <form role="form" action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $singleItem["id"]?>">
        <div class="form-group">
            <label >Name</label>
            <input type="text" class="form-control" id="name" value="<?php echo $singleItem["name"]?>" name="name">
        </div>
        <div class="form-group">
            <label>Birthday:</label>
            <input type="date" class="form-control" name="Birthday"  value="<?php echo $singleItem["birthdate"]?>" >
        </div>
            <input type="submit" value="Update">

        </div>
    </form>
</div>

</body>
</html>


