<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select City</h2>
    <form role="form" action="store.php" method="post">
        <div class="form-group">
            <div class="form-group">
                <label >Name</label>
                <input type="text" class="form-control" id="name" placeholder="Enter your name" name="name">
            </div>
            <label for="sel1">Select list (select one):</label>
            <select class="form-control" name="City" id="sel1" >
                <option >Dhaka</option>
                <option >Chittagong</option>
                <option >Maijdee</option>
                <option >Comilla</option>
                <option >Cox's Bazar</option>
                <option >Khulna</option>
                <option >Faridpur</option>
                <option >Jamalpur</option>
                <option >Feni</option>
                <option >Sylhet</option>
                <option >Barishal</option>
                <option >Rajshahi</option>
                <option >Rangpur</option>
                <option >Gagipur</option>
            </select><br>
            <input type="submit" value="Submit">

        </div>
    </form>
</div>

</body>
</html>

