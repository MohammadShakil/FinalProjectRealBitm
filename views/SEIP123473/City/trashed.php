<?php
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\City\City;

$city=new City();
$trashedItems=$city->trashed();

?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <h2>All city list</h2>
    <a href="index.php" class="btn btn-info" role="button">View Index List</a><br><br>



    <form action="recoverMultiple.php" method="post" id="multiple">
        <button type="submit" class="btn btn-primary">Recover Seleted Item</button>
        <button type="submit" class="btn btn-danger" id="delete">Delete Seleted Item</button><br><br>
        <div class="table-responsive">

            <table class="table">
                <thead>
                <tr>
                    <th>SL#</th>
                    <th>ID</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>

                <?php
                $s=0;
                foreach($trashedItems as $trashed){
                    $s++;
                    ?>
                    <tr>
                        <td><input type="checkbox" name="mark[]" value="<?php echo $trashed["id"]?>"></td>
                        <td><?php echo $s?></td>
                        <td><?php echo $trashed['id']?></td>
                        <td><?php echo $trashed['name']?></td>
                        <td><?php echo $trashed['city_name']?></td>
                        <td>

                            <a href="recover.php?id=<?php echo $trashed["id"]?>" class="btn btn-warning" role="button">Recover</a>
                            <a href="delete.php?id=<?php echo $trashed["id"]?>" class="btn btn-danger" role="button">Delete</a>
                        </td>
                    </tr>

                <?php }?>

                </tbody>
            </table>
        </form>
    </div>
</div>

<script>
    $('#delete').on('click',function(){
        document.forms[0].action="deleteMultiple.php";
        $('#multiple').submit();
    });
</script>

</body>




</html>


