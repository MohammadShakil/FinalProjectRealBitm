<?php

include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\City\City;
$city=new City();
$singleItem=$city->prepare($_GET)->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>City</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Select City</h2>
    <form role="form" action="update.php" method="post">
        <input type="hidden" name="id" value="<?php echo $singleItem["id"]?>">
        <div class="form-group">
            <div class="form-group">
                <label >Name</label>
                <input type="text" class="form-control" id="name" value="<?php echo $singleItem["name"]?>" name="name">
            </div>
            <label for="sel1">Select list (select one):</label>
            <select class="form-control" name="City" >
                <option   value="Dhaka" <?php if(in_array("Dhaka",$singleItem)){echo"selected";}else{"";}?>>Dhaka</option>
                <option  value="Chittagong" <?php if(in_array("Chittagong",$singleItem)){echo"selected";}else{"";}?>>Chittagong</option>
                <option  value="Noakhali" <?php if(in_array("Noakhali",$singleItem)){echo"selected";}else{"";}?>>Noakhali</option>
                <option  value="Comilla" <?php if(in_array("Comilla",$singleItem)){echo"selected";}else{"";}?>>Comilla</option>
                <option  value="Cox's Bazar" <?php if(in_array("Cox's Bazar",$singleItem)){echo"selected";}else{"";}?>>Cox's Bazar</option>
                <option  value="Khulna" <?php if(in_array("Khulna",$singleItem)){echo"selected";}else{"";}?>>Khulna</option>
                <option  value="Faridpur" <?php if(in_array("Faridpur",$singleItem)){echo"selected";}else{"";}?>>Faridpur</option>
                <option  value="Jamalpur" <?php if(in_array("Jamalpur",$singleItem)){echo"selected";}else{"";}?>>Jamalpur</option>
                <option  value="Feni"<?php if(in_array("Feni",$singleItem)){echo"selected";}else{"";}?> >Feni</option>
                <option  value="Sylhet" <?php if(in_array("Sylhet",$singleItem)){echo"selected";}else{"";}?>>Sylhet</option>
                <option  value="Barishal"<?php if(in_array("Barishal",$singleItem)){echo"selected";}else{"";}?>>Barishal</option>
                <option  value="Rajshahi" <?php if(in_array("Rajshahi",$singleItem)){echo"selected";}else{"";}?>>Rajshahi</option>
                <option  value="Rangpur" <?php if(in_array("Rangpur",$singleItem)){echo"selected";}else{"";}?>>Rangpur</option>
                <option  value="Gazipur" <?php if(in_array("Gazipur",$singleItem)){echo"selected";}else{"";}?>>Gazipur</option>
            </select><br>
            <input type="submit" value="Update">

        </div>
    </form>
</div>

</body>
</html>


