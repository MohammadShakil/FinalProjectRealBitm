<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\Bitm\SEIP123473\Textarea\Summery;
use App\Bitm\SEIP123473\Message\Message;



$textarea=new Summery();
$allInfo=$textarea->index();

?>



<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>
<div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../../../index.php" class="btn btn-danger square-btn-adjust">Home</a> </div>

<div class="container">
    <h2>Summery List</h2>
    <a href="create.php" class="btn btn-info" role="button">Add Summery</a>
    <a href="trashed.php" class="btn btn-primary" role="button">View Trash List</a>

    <div id="message">
        <?php
        if((array_key_exists("message",$_SESSION))&&(!empty($_SESSION["message"]))) {
            echo Message::message();
        }
        ?>
    </div>



    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th width="400px">Summery</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>

            <?php
            $s=0;
            foreach($allInfo as $info){
                $s++;
                ?>
                <tr>
                    <td><?php echo $s?></td>
                    <td><?php echo $info['id']?></td>
                    <td><?php echo $info['name']?></td>
                    <td width="400px"><?php echo $info['summery']?></td>
                    <td><a href="view.php?id=<?php echo $info["id"]?>" class="btn btn-info" role="button">View</a>
                        <a href="edit.php?id=<?php echo $info["id"]?>" class="btn btn-primary" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo $info["id"]?>" class="btn btn-danger" role="button">Delete</a>
                        <a href="trash.php?id=<?php echo $info["id"]?>" class="btn btn-warning" role="button">Trash</a>
                    </td>
                </tr>

            <?php }?>

            </tbody>
        </table>
    </div>
</div>

<script>
    $("#message").show().delay(2000).fadeOut();
</script>

</body>




</html>


