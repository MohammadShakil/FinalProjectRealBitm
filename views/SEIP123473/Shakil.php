<!DOCTYPE html>
<!--
For More Info Email:shakil.khan12000@gmail.com
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        
        <title>MySelf</title>
        <style>
            h1{
        color:white;
        background:black;
        text-align: center;
        font-family: fantasy;
        }
        img{
            width: 400px;
            height: 400px;

        }
        a{
            text-decoration: none;
            color: maroon;
            font-family: fantasy;
             }
             table{
                 border: 1px solid black;
                 color: maroon;
                 font-family: fantasy;

             }
             table tr th{
                border: 1px solid black;
                color: darkblue;
                font-family: fantasy;
                background-color:oldlace;
             }
              table tr td{
                border: 1px solid black;
                color: maroon;
                font-family: fantasy;
                background-color:#ccffff;
             }
             
        </style>
        
    </head>
    <body>
        <h1 title="ShakilKhan">SHAKIL KHAN</h1>
    <div style="color: white;
padding: 15px 50px 5px 50px;
float: right;
font-size: 16px;"><a href="../../index.php" class="btn btn-danger square-btn-adjust">Home</a> </div>
        <a href="https://www.facebook.com/shakilk3" target="blank"><img  src="../../Resources/logo/Shakil.jpg" alt="ShakilKhan" title="ShakilKhan"></a>
      
        <p>Hey, I AM <a href="http://shakilkhan.brandyourself.com/" target="blank"><b><i><u>SHAKIL KHAN</u></i></b></a>. This is my homepage, so I have to say something about myself. 
            Sometimes it is hard to introduce yourself because you know yourself so well that
            you do not know where to start with. Let me give a try to see what kind of image
            you have about me through my self-description. I hope that my impression about myself and
            your impression about me are not so different. 
            Here it goes. I am a person who is positive about every aspect of life.
            There are many things I like to do, to see, and to experience.
            I like to read, I like to write; I like to think, I like to dream;
            I like to talk, I like to listen. I like to see the sunrise in the morning,
            I like to see the moonlight at night; I like to feel the music flowing on my face, 
            I like to smell the wind coming from the ocean. 
            I like to look at the clouds in the sky with a blank mind,
            I like to do thought experiment when I cannot sleep in the middle of the night.
            I like flowers in spring, rain in summer, leaves in autumn, and snow in winter.
            I like to sleep early, I like to get up late; 
            I like to be alone, I like to be surrounded by people. I like countrys peace,
            I like metropolis’ noise; I like the beautiful west lake in Hangzhou, 
            I like the flat cornfield in Champaign. I like delicious food and comfortable shoes;
            I like good books and romantic movies. I like the land and the nature, I like people. And,
            I like to laugh.</p>
      
      
    
    </body>
</html>
